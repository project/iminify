<?php

namespace Drupal\iminify\Commands;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\iminify\IMinifyJs;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class IMinifyJsCommands extends DrushCommands {

  /**
   * The cache.default service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The iminifyJs service.
   *
   * @var \Drupal\iminify\IMinifyJs
   */
  protected $iminifyJs;

  /**
   * IMinifyJsCommands constructor.
   *
   * @param \Drupal\iminify\IMinifyJs $iminifyJs
   *   The iminify service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache.default service.
   */
  public function __construct(IMinifyJs $iminifyJs, CacheBackendInterface $cache) {
    $this->iminifyJs = $iminifyJs;
    $this->cache = $cache;
  }

  /**
   * All Js files minification.
   *
   * @usage drush iminify:js
   *   Js files minification.
   *
   * @command iminify:js
   * @aliases im:js
   */
  public function iminifyJs() {
    $this->output()->writeln('Minifying all Js files...');
    $files = $this->iminifyJs->loadAllFiles();
    foreach ($files as $fid => $file) {
      $status = $this->iminifyJs->minifyFile($fid, FALSE);

      // Only output error messages.
      if ($status !== TRUE) {
        $this->output()->writeln($status);
      }
    }

    $this->cache->delete(IMINIFY_JS_CACHE_CID);

    $this->output()->writeln('Complete!');
  }

  /**
   * Drush command to find all Js files.
   *
   * @usage drush iminify-scan:js
   *   Drush command to find all Js files.
   *
   * @command iminify-scan:js
   * @param whitelist_include Scan whitelist include directory list, divided by ','.
   * @param blacklist_regex Scan blacklist regex directory list, tested with wildcards only *name*, divided by ','.
   * @aliases imsc:js
   */
  public function scanJs($whitelist_include = 'core/,modules/,themes/,libraries/,profiles/', $blacklist_regex = '*node_modules*,*vendor*') {
    $this->output()->writeln('Scanning for Js files...');
    $this->output()
      ->writeln('Whitelist include: \'' . $whitelist_include . '\'');
    $this->output()->writeln('Blacklist regex: \'' . $blacklist_regex . '\'');

    $this->iminifyJs->scan(explode(',', $whitelist_include), str_replace(',', "\n", $blacklist_regex));
    $this->output()->writeln('Complete!');
  }

  /**
   * Drush command to remove all minified Js files.
   *
   * @usage drush iminify-revert:js
   *   Drush command to remove all minified Js files.
   *
   * @command iminify-revert:js
   * @aliases imr:js
   */
  public function revertJs() {
    foreach ($this->iminifyJs->loadAllFiles() as $fid => $file) {
      $this->iminifyJs->removeMinifiedFile($fid);
    }
    $this->cache->delete(IMINIFY_JS_CACHE_CID);
    $this->output()->writeln('Complete!');
  }

  /**
   * Status Js.
   *
   * @usage drush iminify-status:js
   *   Drush command debug minified Js files.
   *
   * @command iminify-status:js
   * @option full Display full list of files
   * @aliases ims:js
   */
  public function statusJs($options = ['full' => FALSE]) {
    $files = $this->iminifyJs->loadAllFiles();
    // Statistics.
    $number_of_files = 0;
    $minified_files = 0;
    $unminified_size = 0;
    $minified_size = 0;
    $saved_size = 0;
    $session = \Drupal::service('tempstore.private')->get('iminify');
    $query = $session->get('query');
    // Filter the files based on query.
    if ($query) {
      $new_files = [];
      foreach ($files as $fid => $file) {
        if (stripos($file->uri, $query) !== FALSE) {
          $new_files[$fid] = $file;
        }
      }
      $files = $new_files;
    }

    if (!empty($files)) {
      // Statistics for all files.
      foreach ($files as $fid => $file) {
        $number_of_files++;
        $unminified_size += $file->size;
        $minified_size += $file->minified_size;
        if ($file->minified_uri) {
          $saved_size += $file->size - $file->minified_size;
          $minified_files++;
        }

        if (FALSE !== $options['full']) {
          $this->output()
            ->writeln(t("@percentage|original @file_url(@size)|original updated @modified|minified @minified_file(@minified)|modification @minified_date",
              [
                '@file_url' => $file->uri,
                '@modified' => date('Y-m-d', $file->modified),
                '@size' => format_size($file->size),
                '@minified' => ($minified_size) ? format_size($saved_size) : 0,
                '@percentage' => ($file->minified_uri && $file->minified_size > 0) ? round(($file->size - $file->minified_size) / $file->size * 100, 2) . '%' : 0 . '%',
                '@minified_date' => $file->minified_modified > 0 ? date('Y-m-d', $file->minified_modified) : '-',
                '@minified_file' => format_size($file->minified_size),
              ]));
        }
      }
    }

    // Report on statistics.
    $this->output()
      ->writeln(t(
      '@files Js files (@min_files minified). The size of all original files is @size and the size of all of the minified files is @minified for a savings of @diff (@percent% smaller overall)',
      [
        '@files' => $number_of_files,
        '@min_files' => $minified_files,
        '@size' => format_size($unminified_size),
        '@minified' => ($minified_size) ? format_size($minified_size) : 0,
        '@diff' => ($minified_size) ? format_size($saved_size) : 0,
        '@percent' => ($minified_size) ? round($saved_size / $unminified_size * 100, 2) : 0,
      ]
    ));
  }

}
